import * as actionTypes from '../actions/board-actions';
import { arrayWithValue } from '../utils/array-with-value';

export const initialState = [
  [null, null, null],
  [null, null, null],
  [null, null, null],
];


export default (state = initialState, action) => {
  switch (action.type) {

    case actionTypes.SET_SQUARE_VALUE: {

      const updatedRow = arrayWithValue(state[action.row], action.col, action.value);
      const updatedBoard = arrayWithValue(state, action.row, updatedRow);

      return updatedBoard;
    }

    case actionTypes.RESET_BOARD:
      return initialState;

    default:
      return state;
  }
};
