import playersReducer from './players-reducer';
import * as actionTypes from '../actions/players-actions';

describe('players reducer', () => {

  const initialState = {
    players: [
      {
        id: 0,
        value: 'X',
        victorious: false,
      },
      {
        id: 1,
        value: 'O',
        victorious: false,
      },
    ],
    activePlayer: 0
  };

  it('should return the initial state', () => {
    expect(playersReducer(undefined, {})).toEqual(initialState);
  });

  it('should handle TOGGLE_ACTIVE_PLAYER', () => {
    const action = {
      type: actionTypes.TOGGLE_ACTIVE_PLAYER,
    };
    const expectedState = {
      players: [
        {
          id: 0,
          value: 'X',
          victorious: false,
        },
        {
          id: 1,
          value: 'O',
          victorious: false,
        },
      ],
      activePlayer: 1,
    };

    expect(playersReducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle CHECK_FOR_VICTORY', () => {
    const board = [
      ['X', 'X', 'X'],
      ['O', 'X', 'O'],
      ['X', 'O', 'O'],
    ];
    const action = {
      type: actionTypes.CHECK_FOR_VICTORY,
      board: board,
    };

    const expectedState = {
      players: [
        {
          id: 0,
          value: 'X',
          victorious: true,
        },
        {
          id: 1,
          value: 'O',
          victorious: false,
        },
      ],
      activePlayer: 0,
    };

    expect(playersReducer(initialState, action)).toEqual(expectedState);
  });

});
