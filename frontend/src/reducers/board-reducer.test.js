import boardReducer from './board-reducer';
import * as actionTypes from '../actions/board-actions';

describe('board reducer', () => {

  const initialState = [
    [null, null, null],
    [null, null, null],
    [null, null, null],
  ];

  it('should return the initial state', () => {
    expect(boardReducer(undefined, {})).toEqual(initialState);
  });

  it('should handle SET_SQUARE_VALUE', () => {
    const action = {
      type: actionTypes.SET_SQUARE_VALUE,
      row: 0,
      col: 1,
      value: 'O',
    };

    const expectedState = [
      [null, 'O', null],
      [null, null, null],
      [null, null, null],
    ];

    expect(boardReducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle RESET_BOARD', () => {
    const action = {
      type: actionTypes.RESET_BOARD,
    };

    const modifiedState = [
      [null, 'O', null],
      [null, null, null],
      [null, null, null],
    ];

    expect(boardReducer(modifiedState, action)).toEqual(initialState);
  });
});
