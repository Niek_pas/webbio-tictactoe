import * as actionTypes from '../actions/players-actions';
import checkWinner from '../utils/check-winner';

export const initialState = {
  players: [
    {
      id: 0,
      value: 'X',
      victorious: false,
    },
    {
      id: 1,
      value: 'O',
      victorious: false,
    },
  ],
  activePlayer: 0,
};

export default (state = initialState, action) => {
  switch (action.type) {

    case actionTypes.TOGGLE_ACTIVE_PLAYER:
      return {
        ...initialState,
        activePlayer: (state.activePlayer === 0) ? 1 : 0
      };

    case actionTypes.CHECK_FOR_VICTORY: {
      const players = state.players.map(player => {
        return {
          ...player,
          victorious: checkWinner(action.board, player.value),
        };
      });
      return {
        ...state,
        players: players,
      };
    }

    default:
      return state;

  }
};
