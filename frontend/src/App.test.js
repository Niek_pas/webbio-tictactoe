import React from 'react';
import App from './App';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';
import boardReducer from './reducers/board-reducer';
import playersReducer from './reducers/players-reducer';

/**
 * Sets up the Enzyme wrapper and Redux store for testing
 */
function setup() {

  Enzyme.configure({ adapter: new Adapter() });

  const combinedReducers = combineReducers({
    board: boardReducer,
    players: playersReducer,
  });
  const store = createStore(combinedReducers);

  const enzymeWrapper = mount(
    <Provider store={store}>
      <App />
    </Provider>
  );

  return {
    store,
    enzymeWrapper
  };
}

it('should create the app', () => {
  const { enzymeWrapper } = setup();
  expect(enzymeWrapper.find('App')).toBeDefined();
});
