import React from 'react';
import PropTypes from 'prop-types';

const VictoryMessage = ({ player, draw }) => {
  if (draw) {
    return <p>It&apos;s a draw!</p>;
  }
  return player ? <p>Player {player.value} has won!</p> : <p></p>;
};

VictoryMessage.propTypes = {
  player: PropTypes.shape({
    id: PropTypes.number.isRequired,
    value: PropTypes.string.isRequired,
    victorious: PropTypes.bool.isRequired,
  }),
  draw: PropTypes.bool,
};

export default VictoryMessage;
