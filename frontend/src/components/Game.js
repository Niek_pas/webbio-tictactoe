import React from 'react';
import './Game.css';
import PropTypes from 'prop-types';
import VictoryMessage from './VictoryMessage';
import postBoard from '../utils/post-board';

class Game extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      victoriousPlayer: null,
    };

    this.setSquareValue = this.setSquareValue.bind(this);
  }

  componentDidUpdate(prevProps) {
    // If the board has changed, check the board for a victory
    if (this.props.board !== prevProps.board) {
      this.props.checkForVictory(this.props.board);
    }
    // If the players (or their 'victorious' prop) have changed, get the victorious player
    // and update state
    if (this.props.players !== prevProps.players) {
      this.handlePlayersChanged();
    }

    // If the board is full and there are no victories, consider the game a draw
    if (this.boardFull(this.props.board) && !this.state.draw) {
      this.setState({
        draw: true,
      });
      this.postBoard(this.props.board);
    }
  }

  handlePlayersChanged() {
    const victoriousPlayer = this.props.players.find(player => player.victorious);

    // If there is a victory, update the state and show message, and post the result
    if (victoriousPlayer) {
      this.setState({
        victoriousPlayer: victoriousPlayer,
      });
      this.postBoard(this.props.board);
      return;
    }
  }

  setSquareValue = (col, row) => {
    // If the square already has a value, don't do anything
    if (this.props.board[row] && this.props.board[row][col]) {
      return;
    }
    const activePlayer = this.props.players.find(player => player.id === this.props.activePlayer);
    this.props.setSquareValue(activePlayer.value, col, row);
    this.props.toggleActivePlayer();
  };

  boardFull = (board) => {
    for (let i = 0; i < board.length; i++) {
      const row = board[i];
      for (let j = 0; j < row.length; j++) {
        const col = row[j];
        if (!col) {
          return false;
        }
      }
    }
    return true;
  }

  postBoard = async (board) => {
    postBoard(board);
  }

  render() {
    return (
      <div className="game">
        <VictoryMessage player={this.state.victoriousPlayer} draw={this.state.draw} />
        <table className="tic-tac-toe-board">
          <tbody>
            {this.props.board.map((row, i) => {
              return (
                <tr key={i}>
                  {row.map((col, j) => {
                    return (
                      <td
                        onClick={
                          () => { this.setSquareValue(j, i); }
                        }
                        key={j}>
                        {col}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
        <div></div>
      </div>
    );
  }
}

Game.propTypes = {
  // STATE
  board: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)).isRequired,
  players: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    value: PropTypes.string.isRequired,
    victorious: PropTypes.bool.isRequired,
  })).isRequired,
  activePlayer: PropTypes.number.isRequired,

  // DISPATCH
  setSquareValue: PropTypes.func.isRequired,
  toggleActivePlayer: PropTypes.func.isRequired,
  resetBoard: PropTypes.func.isRequired,
  checkForVictory: PropTypes.func.isRequired,
};

export default Game;
