import React from 'react';
import GameContainer from '../containers/GameContainer';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';
import boardReducer from '../reducers/board-reducer';
import playersReducer from '../reducers/players-reducer';
/**
 * Sets up the Enzyme wrapper and Redux store for testing
 */
function setup() {

  Enzyme.configure({ adapter: new Adapter() });

  const combinedReducers = combineReducers({
    board: boardReducer,
    players: playersReducer,
  });
  const store = createStore(combinedReducers);

  const enzymeWrapper = mount(
    <Provider store={store}>
      <GameContainer />
    </Provider>
  );

  return {
    store,
    enzymeWrapper
  };
}

describe('Game component test', () => {
  const { enzymeWrapper } = setup();

  it('should render the game board', () => {
    const board = enzymeWrapper.find('table');
    expect(board.exists()).toBe(true);
    expect(board.hasClass('tic-tac-toe-board')).toBe(true);
  });

  it('should place an X when the board is clicked once', () => {
    const square = enzymeWrapper.find('td').first();
    expect(square.exists()).toBe(true);
    square.props().onClick();
    expect(square.text()).toBe('X');
  });

  it('should place an O when the board is clicked a second time', () => {
    expect(enzymeWrapper.find('table').hasClass('tic-tac-toe-board')).toBe(true);
    const square = enzymeWrapper.find('td').first();
    const secondSquare = enzymeWrapper.find('td').last();
    expect(square.exists()).toBe(true);
    expect(secondSquare.exists()).toBe(true);
    square.props().onClick();
    secondSquare.props().onClick();
    expect(secondSquare.text()).toBe('O');
  });

});
