import { connect } from 'react-redux';
import Game from '../components/Game';
import { setSquareValue, resetBoard } from '../action-creators/board-action-creators';
import { toggleActivePlayer, checkForVictory } from '../action-creators/players-action-creators';

const mapStateToProps = state => {
  return {
    board: state.board,
    players: state.players.players,
    activePlayer: state.players.activePlayer,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setSquareValue: (value, col, row) => {
      dispatch(setSquareValue(value, col, row));
    },
    resetBoard: () => {
      dispatch(resetBoard());
    },
    toggleActivePlayer: () => {
      dispatch(toggleActivePlayer());
    },
    checkForVictory: (board) => {
      dispatch(checkForVictory(board));
    }
  };
};


const GameContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Game);

export default GameContainer;
