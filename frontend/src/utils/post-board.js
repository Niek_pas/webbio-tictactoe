export default async (board) => {
  const url = 'http://localhost:8080/board';
  const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  };
  const body = JSON.stringify({ board: board });

  const rawResponse = await fetch(url, {
    method: 'POST',
    headers: headers,
    body: body
  });
  return await rawResponse.status;
};
