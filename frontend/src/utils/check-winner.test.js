import checkWinner from './check-winner';

describe('check winner test', () => {

  it('should return true for a win by row', () => {
    const board = [
      ['X', 'O', 'O'],
      ['X', 'X', 'X'],
      ['X', 'O', 'O'],
    ];
    const char = 'X';

    expect(checkWinner(board, char)).toEqual(true);
  });

  it('should return true for a win by row when checking for a different value', () => {
    const board = [
      ['X', 'O', 'O'],
      ['O', 'O', 'O'],
      ['X', 'X', 'O'],
    ];
    const char = 'O';

    expect(checkWinner(board, char)).toEqual(true);
  });

  it('should return true for a win by row when not the entire board is filled', () => {
    const board = [
      ['X', 'O', null],
      ['X', 'X', 'X'],
      ['X', null, 'O'],
    ];
    const char = 'X';

    expect(checkWinner(board, char)).toEqual(true);
  });

  it('should return true for a win by column', () => {
    const board = [
      ['X', 'X', 'O'],
      ['O', 'X', 'X'],
      ['X', 'X', 'O'],
    ];
    const char = 'X';

    expect(checkWinner(board, char)).toEqual(true);
  });

  it('should return true for a win by diagonal', () => {
    const board = [
      ['X', 'X', 'O'],
      ['O', 'X', 'O'],
      ['X', 'O', 'X'],
    ];
    const char = 'X';

    expect(checkWinner(board, char)).toEqual(true);
  });

  it('should return true for a win by reverse diagonal', () => {
    const board = [
      ['X', 'O', 'X'],
      ['O', 'X', 'O'],
      ['X', 'O', 'O'],
    ];
    const char = 'X';

    expect(checkWinner(board, char)).toEqual(true);
  });

  it('should return false for a win by the other player', () => {
    const board = [
      ['X', 'O', 'O'],
      ['O', 'X', 'O'],
      ['X', 'O', 'O'],
    ];
    const char = 'X';

    expect(checkWinner(board, char)).toEqual(false);
  });

  it('should return false for a draw', () => {
    const board = [
      ['X', 'O', 'X'],
      ['X', 'O', 'O'],
      ['O', 'X', 'X'],
    ];
    const char = 'X';

    expect(checkWinner(board, char)).toEqual(false);
  });

  it('should return false for an empty board', () => {
    const board = [
      [null, null, null],
      [null, null, null],
      [null, null, null],
    ];
    const char = 'X';

    expect(checkWinner(board, char)).toEqual(false);
  });

});
