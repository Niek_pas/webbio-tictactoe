export const arrayWithValue =
  (array, index, value) => Object.assign([...array], { [index]: value });
