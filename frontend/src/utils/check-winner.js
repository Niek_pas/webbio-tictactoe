export default (board, char) => {
  if (checkRows(board, char)) {
    return true;
  }
  if (checkColumns(board, char)) {
    return true;
  }
  if (checkDiagonal(board, char)) {
    return true;
  }
  if (checkReverseDiagonal(board, char)) {
    return true;
  }

  return false;
};

const checkRows = (board, char) => {
  let buf = 0;
  let rowIndex = 0;

  while (buf < 3 && rowIndex < 3) {
    const row = board[rowIndex];

    // Check each square in the row
    for (let j = 0; j < row.length; j++) {
      if (checkSquare(row[j], char)) {
        buf++;
        continue;
      }
      buf = 0;
    }

    rowIndex++;
  }
  return buf >= 3;
};

const checkColumns = (board, char) => {
  let buf = 0;
  let colIndex = 0;

  while (buf < 3 && colIndex < 3) {
    // Check each square in the column
    for (let j = 0; j < board.length; j++) {
      if (checkSquare(board[j][colIndex], char)) {
        buf++;
        continue;
      }
      buf = 0;
    }

    colIndex++;
  }
  return buf >= 3;
};

const checkDiagonal = (board, char) => {
  let buf = 0;
  let rowIndex = 0;
  let colIndex = 0;

  while (buf < 3 && rowIndex < 3 && colIndex >= 0) {
    // Check each square in the diagonal bottom left - top right
    buf = (checkSquare(board[rowIndex][colIndex], char)) ?
      buf + 1 :
      0;

    rowIndex++;
    colIndex++;
  }
  return buf >= 3;
};

const checkReverseDiagonal = (board, char) => {
  let buf = 0;
  let rowIndex = 0;
  let colIndex = board.length - 1;

  while (buf < 3 && rowIndex < 3 && colIndex >= 0) {
    // Check each square in the diagonal bottom left - top right
    buf = (checkSquare(board[rowIndex][colIndex], char)) ?
      buf + 1 :
      0;

    rowIndex++;
    colIndex--;
  }
  return buf >= 3;
};

const checkSquare = (square, char) => {
  if (square === char) {
    return true;
  }
  return false;
};
