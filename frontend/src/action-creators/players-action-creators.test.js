import * as actionCreators from './players-action-creators';
import * as actionTypes from '../actions/players-actions';

describe('board actions creators test', () => {

  it('should create an action to toggle the active player', () => {
    const expectedAction = {
      type: actionTypes.TOGGLE_ACTIVE_PLAYER,
    };
    expect(actionCreators.toggleActivePlayer()).toEqual(expectedAction);
  });

  it('should create an action to check for victory', () => {
    const board = [
      ['X', 'X', 'X'],
      ['O', 'X', 'O'],
      ['X', 'O', 'O'],
    ];
    const expectedAction = {
      type: actionTypes.CHECK_FOR_VICTORY,
      board: board,
    };

    expect(actionCreators.checkForVictory(board)).toEqual(expectedAction);
  });
});
