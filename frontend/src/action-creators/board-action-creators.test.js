/* eslint max-len: 0 */
// Eslint complains about lines with it() calls being too long,
// but complains about indentation when splitting the names over several lines
import * as actionCreators from './board-action-creators';
import * as actionTypes from '../actions/board-actions';

describe('board actions creators test', () => {

  it('should create an action to set a square value', () => {
    const row = 0;
    const col = 1;
    const value = '0';

    const expectedAction = {
      type: actionTypes.SET_SQUARE_VALUE,
      row: row,
      col: col,
      value: value,
    };
    expect(actionCreators.setSquareValue(value, col, row)).toEqual(expectedAction);
  });

  it('should throw when trying to create an action to set a square value without value defined', () => {
    const row = 0;
    const col = 1;
    const value = undefined;
    const errorMessage = 'setSquareValue(): failed to create action; value is not defined.';
    expect(actionCreators.setSquareValue.bind(this, value, col, row)).toThrowError(errorMessage);
  });

  it('should throw when trying to create an action to set a square value with a value of the incorrect type', () => {
    const row = 0;
    const col = 1;
    const value = true;
    const errorMessage = 'setSquareValue(): failed to create action; ' +
      'expected value to be a string, but it is a boolean.';
    expect(actionCreators.setSquareValue.bind(this, value, col, row)).toThrowError(errorMessage);
  });

  it('should throw when trying to create an action to set a square value without row defined', () => {
    const row = undefined;
    const col = 1;
    const value = '0';
    const errorMessage = 'setSquareValue(): failed to create action; row is not defined.';
    expect(actionCreators.setSquareValue.bind(this, value, col, row)).toThrowError(errorMessage);
  });

  it('should throw when trying to create an action to set a square value with a row of the incorrect type', () => {
    const row = 'your boat';
    const col = 1;
    const value = '0';
    const errorMessage = 'setSquareValue(): failed to create action; ' +
      'expected row to be a number, but it is a string.';
    expect(actionCreators.setSquareValue.bind(this, value, col, row)).toThrowError(errorMessage);
  });

  it('should throw when trying to create an action to set a square value without col defined', () => {
    const row = 0;
    const col = undefined;
    const value = '0';
    const errorMessage = 'setSquareValue(): failed to create action; col is not defined.';
    expect(actionCreators.setSquareValue.bind(this, value, col, row)).toThrowError(errorMessage);
  });

  it('should throw when trying to create an action to set a square value with a col of the incorrect type', () => {
    const row = 0;
    const col = 'on me';
    const value = '0';
    const errorMessage = 'setSquareValue(): failed to create action; ' +
      'expected col to be a number, but it is a string.';
    expect(actionCreators.setSquareValue.bind(this, value, col, row)).toThrowError(errorMessage);
  });

  it('should create an action to reset the board', () => {
    const expectedAction = {
      type: actionTypes.RESET_BOARD,
    };
    expect(actionCreators.resetBoard()).toEqual(expectedAction);
  });

});
