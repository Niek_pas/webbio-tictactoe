import * as actionTypes from '../actions/board-actions';

/**
 * Creates an action to set the value of a given square
 * @param {string} value The value to set the square to. Should be either 'X' or 'O'.
 * @param {int} col The (zero-indexed) column coordinate of the square.
 * @param {int} row The (zero-indexed) row coordinate of the square.
 */
export function setSquareValue(value, col, row) {
  if (col === null || col === undefined) {
    throw new TypeError('setSquareValue(): failed to create action; col is not defined.');
  }
  if (typeof col !== 'number') {
    throw new TypeError('setSquareValue(): failed to create action; ' +
      `expected col to be a number, but it is a ${typeof col}.`);
  }

  if (row === null || row === undefined) {
    throw new TypeError('setSquareValue(): failed to create action; row is not defined.');
  }
  if (typeof row !== 'number') {
    throw new TypeError('setSquareValue(): failed to create action; ' +
      `expected row to be a number, but it is a ${typeof row}.`);
  }

  if (value === null || value === undefined) {
    throw new TypeError('setSquareValue(): failed to create action; value is not defined.');
  }
  if (typeof value !== 'string') {
    throw new TypeError('setSquareValue(): failed to create action; expected ' +
      `value to be a string, but it is a ${typeof value}.`);
  }

  return {
    type: actionTypes.SET_SQUARE_VALUE,
    col: col,
    row: row,
    value: value,
  };
}

/**
 * Creates an action to reset the board, setting the values of all the squares to null
 */
export function resetBoard() {
  return {
    type: actionTypes.RESET_BOARD,
  };
}
