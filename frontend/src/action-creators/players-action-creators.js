import * as actionTypes from '../actions/players-actions';

/**
 * Creates an action to toggle which player's turn it is
 */
export function toggleActivePlayer() {
  return {
    type: actionTypes.TOGGLE_ACTIVE_PLAYER,
  };
}

/**
 * Creates an action to check wheter any of the players have 
 * obtained victory in the specified board state
 * @param {string[][]} board The board against which to match all players for victory
 */
export function checkForVictory(board) {
  if (!board) {
    throw new TypeError('checkBoard(): failed to create action; board is not defined.');
  }

  if (board.length < 1) {
    throw new TypeError('checkBoard(): failed to create action; board is empty.');
  }

  return {
    type: actionTypes.CHECK_FOR_VICTORY,
    board: board
  };
}
