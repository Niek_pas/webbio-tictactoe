import { createStore, combineReducers } from 'redux';
import { devToolsEnhancer } from 'redux-devtools-extension';
import boardReducer from './reducers/board-reducer';
import playersReducer from './reducers/players-reducer';

const rootReducer = combineReducers({ board: boardReducer, players: playersReducer });

export default createStore(rootReducer, devToolsEnhancer());
