De frontend van dit project is geschreven in React/Redux, met name omdat, naar mijn idee, deze 
technologieën (m.n. Redux) ideaal zijn voor een turn-based spel als tic tac toe: alles wat er
gebeurt is een duidelijk afgebakende actie die dan ook correspondeert met een singuliere Redux 
actie. De backend is een eenvoudige Node Express app, die eigenlijk niet veel meer doet dan
JSON ontvangen en stringified opslaan in een MongoDB database.

Om dit project te runnen is een lokale installatie van MongoDB vereist. Hiernaast moet in 
zowel de ./frontend/ als de ./backend/ map `npm install` of `yarn` uitgevoerd worden om de 
dependencies te installeren. Om de backendserver te starten moet simpelweg `node index.js` 
uitgevoerd worden; om de frontend te starten moet `yarn start` uitgevoerd worden.

Ik heb ervoor te kiezen de backend en frontend in één git repository onder te brengen omdat 
dit iets praktischer te delen is, maar in de 'echte' praktijk zou ik dat niet doen.