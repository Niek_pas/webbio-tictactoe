/**
 * Validates the format of a board
 * @param {*} board the board to validate
 */
function boardValid(board) {
  if (!board || !board[0] || !board[0][0]) {
    return false;
  }
  if (board.length < 3 || board[0].length < 3) {
    return false;
  }
  return true;
}

module.exports = boardValid;
