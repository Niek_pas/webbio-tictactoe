const GameResult = require('../models/GameResult');
/**
 * Saves a game board to the database
 * @param {string} board The JSON string of the board to save
 */
async function saveGame(board) {
  const gameResult = new GameResult({
    board: board,
  });

  return new Promise((resolve, reject) => {
    gameResult.save()
      .then(() => {
        resolve();
      }, err => {
        reject(err);
      });
  });
}

module.exports = saveGame;
