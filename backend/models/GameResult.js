const mongoose = require('mongoose');
const gameResultSchema = new mongoose.Schema({
  board: {
    type: String,
    required: true,
  },
});

const GameResult = mongoose.model('GameResult', gameResultSchema);

module.exports = GameResult;
