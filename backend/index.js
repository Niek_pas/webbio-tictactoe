const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const winston = require('winston');
const saveGame = require('./helpers/save-game');
const boardValid = require('./helpers/board-valid');

const app = express();
app.use(bodyParser.json());

dotenv.load();

mongoose.connect(process.env.DATABASE_URL).catch(() => {
  winston.error('Mongodb connection error, is mongod running?');
  process.exit(1);
});

// Allow CORS from all origins for the purposes of this demo
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.post('/board', (req, res) => {
  const board = req.body.board;
  if (!boardValid(board)) {
    // 400 Bad Request
    res.sendStatus(400);
  }
  const boardString = JSON.stringify(board);
  saveGame(boardString)
    .then(() => {
      // 204 No Content
      res.sendStatus(204);
    })
    .catch(() => {
      // 500 Internal Server Error
      res.sendStatus(500);
    });
});

const port = process.env.PORT || 8080;
app.listen(port);
